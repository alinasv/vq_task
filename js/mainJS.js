$(function () {

    const TEMPLATE_FB = $("#first_block").html();
    const TEMPLATE_EB = $("#error_block").html();
    const TEMPLATE_RB = $("#result_block").html();
    const FORM_TEMPLATE = $("#second_block").html();

    const $list = $("#list");

    function addData() {

        const $node_fb = $(TEMPLATE_FB);
        const $node_sb = $(FORM_TEMPLATE);
        const $node_rb = $(TEMPLATE_RB);
        const $node_eb = $(TEMPLATE_EB);

        const $submitBtn = $node_sb.find("#submitBtn");
        const $resetBtn = $node_sb.find("#resetBtn");

        const $error_text = $node_eb.find("#error_text");
        const $result_text = $node_rb.find("#result_text");

        $submitBtn.click(function () {

            if ($("#arrayInput").val() === "") {
                $node_fb.css("display", "none");
                const $err_empty_msg = "Array can not be empty. \n" +
                    "Please, try again after \"Reset\".";
                $error_text.text($err_empty_msg);
                $submitBtn.prop('disabled', true);
                $list.append($node_eb);
            }
            else {
                const dataArray = $("#my_form").serializeArray(),
                    dataObj = {};

                $(dataArray).each(function (i, field) {
                    dataObj[field.name] = field.value;
                });
                const Res = checkArrayInput(dataObj['arrayInput']);

                if (Res.res === "Result") {
                    $node_fb.css("display", "none");
                    $result_text.text(Res.result_msg + " " + Res.result_arr);

                    $submitBtn.prop('disabled', true);
                    $list.append($node_rb);
                }
                else {
                    $node_fb.css("display", "none");
                    $error_text.text(Res.result_msg);
                    $submitBtn.prop('disabled', true);
                    $list.append($node_eb);
                }

            }

        });

        $resetBtn.click(function () {
            location.reload();
        });

        $list.append($node_fb);
        $list.append($node_sb);
    }

    addData();

    /*
      @function that check and validate input.
      Result is saved in object Res, where field Res.res specifies whether the input gives
      successful result of error;
      Res.result_msg contains the possible error message;
    * */
    function checkArrayInput($carsArray) {
        let Res = {};

         if (hasSpecSymbols($carsArray)) {
            Res["res"] = "Error";
            Res["result_msg"] = "Array elements must be separated only my commas \",\" other symbols are prohibited.\n" +
                "Please, try again after \"Reset\".";
        }
        else if (hasLetters($carsArray)) {
            Res["res"] = "Error";
            Res["result_msg"] = "Letters in array are prohibited. \n" +
                "Please, try again after \"Reset\".";
        }
        else if (exceedsValidNumbers($carsArray)) {
            Res["res"] = "Error";
            Res["result_msg"] = "Array elements must be only numbers 0 and 1. \n" +
                "Please, try again after \"Reset\".";
        }
        else if(checkForOneSymbOnly($carsArray)){
            Res["res"] = "Error";
            Res["result_msg"] = "Where should be at least two cars to count possibility of passing.\n" +
                "Please, try again after \"Reset\".";
        }
        else {
            Res = countCars($carsArray);
        }

        return Res;
    }

    /*
     @function to counts cars;
     * */
    function countCars($carsArray) {

        let $count = 0;

        let $cleanArr  = [];
        for(let i = 0; i < $carsArray.length; i++){
            if($carsArray[i] !== ","){
                $cleanArr.push($carsArray[i]);
            }
        }

        let $cars = [];

        for (let i = 0; i < $cleanArr.length; i++) {
            let $index = $cleanArr[i];
            for (let j = i + 1; j < $cleanArr.length; j++) {
                if ($cleanArr[j] > $index) {
                    $count++;
                    $cars.push("("+i+" "+j + ")");
                }
            }
        }
        let Res = {
            res: "Result",
            result_msg: "Cars amount is " + $count + ": ",
            result_arr: $cars + " "
        }
        return Res;
    }

    /* === Help Function === */

    function checkForOneSymbOnly($input){
        return /^[0-1]$/.test($input);
    }

    /*
     @function to check whether in array parameter consists of
     any characters except comma;
    * */
    function hasSpecSymbols($input) {
        const $spesSymb = "<>@!#$%^&*()_+[]{}?:;|'\"\\./~`-=";
        for (let i = 0; i < $spesSymb.length; i++) {
            if ($input.indexOf($spesSymb[i]) > -1)
                return true;
        }
        return false;
    }

    /*
    @function to check whether in array parameter consists of letters
    * */
    function hasLetters($input) {
        const $consistsLett = /[a-zA-Zа-яА-я]/.test($input);
        return $consistsLett;
    }

    /*
    @function to check whether in array parameter consists of
     numbers other then 0 and 1;
    * */
    function exceedsValidNumbers($input) {
        for (let i = 0; i < $input.length; i++) {
            if ($input[i] < 0 || $input[i] > 1) {
                return true;
            }
        }
        return false;
    }

});
Welcome to my small web app task "VQ task" done with node.js

@Description
The main purpose of this project is to create the single page app which visually and interactively solves the next problem:

A non-empty array A consisting of N integers. Each elements of array A represent a cars on a road. Array A contains only zero or one: 
? 0 represents a car traveling east ( to the right on the X axis); 
? 1 represents a car traveling west ( to the left on the X axis); 
You should count passing cars. We say that a pair of cars (P, Q), where 0 ? P < Q < N, is passing when P is traveling to the east and Q is traveling to the west. 
? For example, consider array A = [0,1,0,1,1] of 5 cars. We have five pairs of passing cars: (0, 1), (0, 3), (0, 4), (2, 3), (2, 4).

@Version 1.0.0

@Structure:
  Main file: index.html where html file structure described;
  JS: runs under js/mainJS.js with all logic and functions;
  CSS: runs under styles/styles.css
  package.json: file for node structure and script configuration;
  
